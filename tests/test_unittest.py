import relstorage
import ZEO
import ZODB

import sheraf


class TestDatabase(sheraf.DatabaseTestCase):
    def test_database(self):
        assert isinstance(self.database, sheraf.databases.Database)
        assert isinstance(self.database.storage, ZODB.DemoStorage.DemoStorage)
        with sheraf.connection(commit=True) as conn:
            conn.root()["foobar"] = True


class TestFileStorage(sheraf.FileStorageTestCase):
    def test_filestorage_database(self):
        assert isinstance(self.database, sheraf.databases.Database)
        assert isinstance(self.database.storage, ZODB.FileStorage.FileStorage)
        with sheraf.connection(commit=True) as conn:
            conn.root()["foobar"] = True


class TestConnected(sheraf.ConnectedTestCase):
    def test_connected(self):
        assert isinstance(self.connection, ZODB.Connection.Connection)
        sheraf.Database.current_connection().root()["foobar"] = True
        sheraf.Database.current_connection().transaction_manager.commit()


class TestRelStorage(sheraf.PostgreSQLRelStorageTestCase):
    def test_relstorage_database(self):
        assert isinstance(self.database, sheraf.databases.Database)
        assert isinstance(self.database.storage, relstorage.storage.RelStorage)
        with sheraf.connection(commit=True) as conn:
            conn.root()["foobar"] = True


class TestZeo(sheraf.ZeoTestCase):
    def test_zeo_database(self):
        assert isinstance(self.database, sheraf.databases.Database)
        assert isinstance(self.database.storage, ZEO.ClientStorage.ClientStorage)
        with sheraf.connection(commit=True) as conn:
            conn.root()["foobar"] = True
